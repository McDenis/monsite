$(document).ready(function() {
    //function open modal
    $('#open_modal').click(function () {
        $('#modal_to_open').css(
            {
                'display': 'block'
            }
        )
    });

    //function close modal
    $('#close_modal').click(function () {
        $('#modal_to_open').css(
            {
                'display': 'none'
            }
        )
    });
});

// Menu change color
$(function () {
    $(document).scroll(function () {
        var $nav = $(".header");
        var $banner = $('.banner');
        $nav.toggleClass('scrolled', $(this).scrollTop() > $banner.height() - 80);
    });
});

//Change to night mode
document.getElementById("night_mode").addEventListener("change", function() {
    if (document.getElementById("night_mode").checked == true) {
        document.documentElement.setAttribute("style", "background-color: black;");
        document.getElementsByTagName("body")[0].setAttribute("style", "color: white;");
        document.getElementById("about-heading-site").setAttribute("style", "color: #95a5a6");
        document.getElementById("portfolio-heading-site").setAttribute("style", "color: #95a5a6");
    } 
    else {
        document.documentElement.setAttribute("style", "background-color: white;");
        document.getElementsByTagName("body")[0].setAttribute("style", "color: black;");
        document.getElementById("about-heading-site").setAttribute("style", "color: #4A4A4A");
        document.getElementById("portfolio-heading-site").setAttribute("style", "color: #4A4A4A");          
    }
});


//Send mail with ajax
$('#send_email').click(function(e){
    e.preventDefault();
    var data = {
        firstname: $('#firstname').val(),
        lastname: $('#lastname').val(),
        email: $('#email').val(),
        subject: $('#subject').val(),
        message: $('#message').val()
    };
    $.ajax({
        url: "mail.php",
        type: 'POST',
        data: data,
        success: function(data) {
            $('#js_alert_success').css({'display' : 'block'});
            setTimeout(function(){
                $('#js_alert_success').css({'display' : 'none'});
                $('#firstname').val("");
                $('#lastname').val("");
                $('#email').val("");
                $('#subject').val("");
                $('#message').val("")
            }, 3000);
            window.alert("Email envoyé.");
            $('#modal_to_open').css(
                {
                    'display': 'none'
                }
            )
        },
        error: function(data) {
            $('#js_alert_danger').css({'display' : 'block'});
            setTimeout(function(){
                $('#js_alert_danger').css({'display' : 'none'});
                $('#firstname').val("");
                $('#lastname').val("");
                $('#email').val("");
                $('#subject').val("");
                $('#message').val("")
            }, 3000);
            window.alert("Une erreur est survenue.");
            $('#modal_to_open').css(
                {
                    'display': 'none'
                }
            )
        }
    });
});